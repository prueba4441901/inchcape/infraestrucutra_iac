data "aws_eks_cluster" "inchcape-cluster" {
  name = "inchcape-cluster"  # Reemplaza con el nombre correcto de tu clúster
}

resource "aws_route53_zone" "inchcape-zone" {
  name = "pruebadevops.com"
}

resource "aws_route53_record" "www"  {
  name    = "pruebadevops.com"
  type    = "A"
  zone_id = aws_route53_zone.inchcape-zone.zone_id
  alias {
    evaluate_target_health = true
    name                   = aws_lb.eks_lb.dns_name
    zone_id                = aws_lb.eks_lb.zone_id
  }
}