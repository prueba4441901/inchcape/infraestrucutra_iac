resource "aws_ecrpublic_repository" "inchcape_repo_public" {
  repository_name = "inchcape-repo-public"
}

output "repository_url" {  
  value = aws_ecrpublic_repository.inchcape_repo_public.repository_uri  
}