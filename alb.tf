resource "aws_lb" "eks_lb" {
  name               = "eks-cluster-lb"
  internal           = false
  load_balancer_type = "application"
  subnets            = module.vpc.public_subnets

  enable_deletion_protection = false
  enable_http2               = true
  enable_cross_zone_load_balancing = true

  tags = {
    Name = "eks-cluster-lb"
  }
}
