module "eks" {
  source = "terraform-aws-modules/eks/aws"
  version = "~>19.0"

  cluster_name = "inchcape-cluster"
  cluster_version = "1.24"

  vpc_id = module.vpc.vpc_id 
  subnet_ids = module.vpc.private_subnets
  cluster_endpoint_public_access = true
  cluster_endpoint_private_access = true

  cluster_addons = {
    coredns= {
        resolve_conflict = "OVERWRITE"
    }
    vpc-cni ={
        resolve_conflict = "OVERWRITE"
    }
    kube-proxy = {
        resolve_conflict = "OVERWRITE"
    }
  }

  eks_managed_node_groups = {
    node-group = {
        desired_capacity = 2
        max_capacity =3
        min_capacity = 2
        instance_types = ["t2.micro"]
        disk_size = 20

        tags = {
            name ="InchcapeNodes"
            terraform = "true"
            Environment = "inchcape-tag"
        }
    }
  }
}
